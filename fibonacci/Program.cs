﻿using System;

namespace fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérlek add meg, hogy hányadik elemét szeretnéd a fibonacci sorozatnak!");
            int place = 0;
            while (!int.TryParse(Console.ReadLine(), out place) || place < 0)
            {
                Console.WriteLine("Kérlek egész számot adj meg, ami nagyobb mint 0!");
            }

            switch (place)
            {
                case 1:
                    Console.WriteLine("A keresett szám: 0");
                    break;
                case 2:
                    Console.WriteLine("A keresett szám: 1");
                    break;
                default:
                    Console.WriteLine(FibonacciService.GetFibonacciWithForLoop(place));
                    Console.WriteLine(FibonacciService.GetFibonacciWithRecursive(place, 0, 0, 1));
                    Console.WriteLine(FibonacciService.GetFibonacciWithWhileLoop(place));
                    break;
            }

            Console.ReadKey();
        }
    }
}