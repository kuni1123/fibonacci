using System;

namespace fibonacci
{
    public static class FibonacciService
    {
        public static string GetFibonacciWithForLoop(int place)
        {
            int n1 = 0, n2 = 1, n3 = 0, i;
            for (i = 2; i <= place; ++i)
            {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
            }

            return "A keresett szám for looppal: " + n3;
        }

        public static string GetFibonacciWithRecursive(int place, int current, int first, int second)
        {
            if (current < place)
            {
                return GetFibonacciWithRecursive(place, current + 1, second, (first + second));
            }

            return "A keresett szám rekurzívan: " + first;
        }

        public static string GetFibonacciWithWhileLoop(int place)
        {
            int n1 = 0, n2 = 1, n3 = 0, i = 2;
            while (i <= place)
            {
                i++;
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
            }

            return "A keresett szám while looppal: " + n3;
        }
    }
}